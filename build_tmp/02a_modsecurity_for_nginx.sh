#!/usr/bin/env bash

# https://www.nginx.com/blog/compiling-and-installing-modsecurity-for-open-source-nginx/

script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
source "${script_dir}/apt_helpers.sh"

nginx_ver=`nginx -v 2>&1 | grep -Po "(?<=nginx version: nginx\/)\d+\.\d+\.\d+"`

echo "[INSTALL MODSECURITY] Install Prerequisite Packages"
apt_get install --yes --no-install-recommends \
  apt-utils \
  autoconf \
  automake \
  build-essential \
  git \
  libcurl4-openssl-dev \
  libgeoip-dev \
  liblmdb-dev \
  libpcre++-dev \
  libtool \
  libxml2-dev \
  libyajl-dev \
  modsecurity-crs \
  pkgconf \
  wget \
  zlib1g-dev


cd /opt \
  && echo "[INSTALL MODSECURITY] Clone ModSecurity v3.0" \
  && git clone \
    --quiet \
    --depth 1 \
    --branch v3/master \
    --single-branch \
    https://github.com/SpiderLabs/ModSecurity > /dev/null \
  && cd ModSecurity > /dev/null \
  && git submodule --quiet init > /dev/null \
  && git submodule --quiet update > /dev/null \
  && echo "[INSTALL MODSECURITY] Build ModSecurity" \
  && ./build.sh > /dev/null \
  && echo "[INSTALL MODSECURITY] Configure ModSecurity" \
  && ./configure > /dev/null \
  && echo "[INSTALL MODSECURITY] Make ModSecurity" \
  && make > /dev/null \
  && echo "[INSTALL MODSECURITY] Make install ModSecurity" \
  && make install > /dev/null

echo "[INSTALL MODSECURITY] Download the NGINX Connector for ModSecurity" \
  && git clone \
    --quiet \
    --depth 1 \
    https://github.com/SpiderLabs/ModSecurity-nginx.git \
  && wget --quiet "http://nginx.org/download/nginx-${nginx_ver}.tar.gz" \
  && tar zxvf "nginx-${nginx_ver}.tar.gz" > /dev/null \
  && cd "nginx-${nginx_ver}" \
  && echo "[INSTALL MODSECURITY] Build the NGINX Connector for ModSecurity" \
  && ./configure --with-compat --add-dynamic-module=../ModSecurity-nginx > /dev/null \
  && make modules > /dev/null \
  && cp objs/ngx_http_modsecurity_module.so /usr/lib/nginx/modules \
  && mkdir /etc/nginx/modsec \
  && cp /opt/ModSecurity/unicode.mapping /etc/nginx/modsec

#!/usr/bin/env bash
dir="$(git rev-parse --show-toplevel)"
var_file="${dir}/local/variables"

if [[ -f "${var_file}" ]]; then
  echo "Loading variables from ${var_file}"
  source "${dir}/local/variables"
else
  echo "Not loading variables from ${var_file}"
fi

# Select the appropriate workspace
terraform workspace list | grep "${TERRAFORM_WORKSPACE}" &> /dev/null
if [[ $? -ne 0 ]]; then
  terraform workspace new "${TERRAFORM_WORKSPACE}"
fi
terraform workspace select "${TERRAFORM_WORKSPACE}"

if [[ $1 -eq "taint" ]]; then
  terraform taint "digitalocean_droplet.fiscal-elk"
fi

terraform plan -out=fiscal-elk.planfile \
  -var "do_token=${DIGITAL_OCEAN_API_KEY}" \
  -var "ci_pvt_key=${DIGITAL_OCEAN_CI_SSH_PRIVATE_KEY}" \
  -var "ci_ssh_fingerprint=${DIGITAL_OCEAN_CI_SSH_FINGERPRINT}" \
  -var "floating_ip=${DIGITAL_OCEAN_ELK_FLOATING_IP}" \
  -var "domain_name=${DIGITAL_OCEAN_ELK_FQDN}" \
  ./terraform

terraform apply -input=false fiscal-elk.planfile \
  `# Strip color codes from output` \
  | sed -r "s/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[mGK]//g" \
  `# Save to log file in local folder` \
  | tee "local/`date -Ins`.log"
